﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GeneratePickUps))]
public class PickUpGenEditor : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GeneratePickUps thisPUGen = (GeneratePickUps)target;

        if (GUILayout.Button("Generate PickUps"))
        {
            thisPUGen.PlaceHPPickups();
        }

        if (GUILayout.Button("Remove PickUps"))
        {
            thisPUGen.ClearPickUps();
        }
    }
}
