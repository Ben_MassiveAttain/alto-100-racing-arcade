﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WaypointGenerator))]
public class WayPointEditor : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        WaypointGenerator thisWPGen = (WaypointGenerator)target;

        if (GUILayout.Button("Calculate Mean Distance"))
        {
            thisWPGen.CalcAverageDist();
        }

        if (GUILayout.Button("Generate WayPoints")) {
            thisWPGen.InstantiateWPs();
        }

        if (GUILayout.Button("Delete Current WayPoints"))
        {
            thisWPGen.RemoveWPs();
        }

        if (GUILayout.Button("Clear"))
        {
            thisWPGen.ClearPos();
        }
    }
}
