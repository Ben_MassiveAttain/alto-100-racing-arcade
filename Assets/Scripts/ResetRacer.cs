﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetRacer : MonoBehaviour {

    public WaypointGenerator WPGen;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 10) {
            RacePosition RP = other.GetComponent<RacePosition>();
            
            other.transform.position = WPGen.WayPoints[RP.WayPointVal].transform.position;

            other.transform.LookAt(WPGen.WayPoints[RP.WayPointVal + 1].transform.position);

            if (other.tag == "AI") {
                other.GetComponent<AIMovement>().MoveNavAgent(RP.WayPointVal);
            }
        }
    }

}
