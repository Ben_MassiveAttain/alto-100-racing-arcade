﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinAnim : MonoBehaviour {

    public GameObject PivOb;

    public float SpeedBal;

    public float RotSpeed = 5.0f;

	// Update is called once per frame
	void FixedUpdate () {
        PivOb.transform.Rotate(Vector3.up * (Time.deltaTime * RotSpeed * SpeedBal), Space.World);
    }


}
