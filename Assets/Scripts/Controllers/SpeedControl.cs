﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alto;
using UnityEngine.AI;

public class SpeedControl : MonoBehaviour {
    
    #region Public Vals
    /*public float MaxIncrease = 0.20f;
    public int IncrementTime = 3;*/
    public float CurrentSpeed;
    #endregion

    /*public float OldMaxSpeed;
    public float NewSpeed;
    public float IncrementVal;*/

    /*public IEnumerator NewSpeedPush()
    {
       ChangeSpeed();

        yield return new WaitForSeconds(IncrementTime);

        ReturnSpeed();
    }*/

    public virtual void ChangeSpeed(int Val){}
    
    public virtual void ReturnSpeed(int Val){}
    
}
