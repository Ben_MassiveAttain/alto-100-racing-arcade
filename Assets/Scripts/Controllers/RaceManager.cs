﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Alto;

public class RaceManager : MonoBehaviour
{
    public static GameObject PlayerOb;

    public static int MaxLapCount = 1;

    public int LapCount = 2;

    public static int WPCount;

    public static float PickUpTime = 10.0f;

    public int RaceDelay = 5;

    static public int MaxRaceConcVal = 3;

    static GameObject ScoreCanvas;

    public static int MaxFinalWPTrig;
    public static int WPTrigVal;

    //public static HealthController HPCont;
    public static RacePosition RP;
    public static SpeedControl SpeedCont;

    // Use this for initialization
    void Start()
    {
        MaxLapCount = LapCount;

        PlayerOb = GameObject.FindGameObjectWithTag("Player");
        ScoreCanvas = GameObject.Find("ScoreCanvas PF");

        //HPCont = RaceManager.PlayerOb.GetComponent<HealthController>();
        RP = RaceManager.PlayerOb.GetComponent<RacePosition>();
        SpeedCont = RaceManager.PlayerOb.GetComponent<SpeedControl>();

        WPCount = WaypointGenerator.WPCountCheck();

        //ScoreCanvas.SetActive(false);
        CalcRaceConVal();

        StartRace();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            ConcludeRace();
        }
    }

    void StartRace()
    {
        RaceManager.PlayerOb.GetComponentInChildren<PlayerInterface>().StartCountDown(RaceDelay);

        Invoke("InitRacers", RaceDelay + 1.0f);

    }

    public void InitRacers()
    {
        for (int i = 0; i < RacePositionManager.RacePositions.Count; i++)
        {
            if (RacePositionManager.RacePositions[i].gameObject.tag == RacePositionManager.RacerTag)
            {
                AIMovement AIM = RacePositionManager.RacePositions[i].gameObject.GetComponent<AIMovement>();

                AIM.MoveNavAgent(0);
            }
        }
        
        AltoRaceController.AllowMovement = true;
    }

    public static void CalcRaceConVal()
    {
        MaxFinalWPTrig = (MaxLapCount * RacePositionManager.RacePositions.Count) - MaxRaceConcVal;
    }

    public static void CheckConc()
    {
        WPTrigVal++;

        //if (WPTrigVal == MaxFinalWPTrig)
        if (WPTrigVal == MaxFinalWPTrig)
        {
            ConcludeRace();
        }
    }

    public static void ConcludeRace()
    {
        Debug.Log("Concluded");

        //ScoreCanvas.SetActive(false);

        ScoreManager.DisableOb(false);

        PlayerInterface.DispStageText("FINISHED");

        RacePositionManager.StopAllRacers();

        for (int i = 0; i < RacePositionManager.RacePositions.Count - 1; i++)
        {
            ScoreManager.NewTextOb(RacePositionManager.RacePositions[i].GetComponent<AIDisplay>().DispName, i + 1);
        }

        ScoreCanvas.SendMessage("LoadNext");
    }

}