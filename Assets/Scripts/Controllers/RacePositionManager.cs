﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Alto;

public class RacePositionManager : MonoBehaviour {

    public static string RacerTag = "Enemy";

    #region Static Vals
    /*public static GameObject[] AIRacers;
    public static List<GameObject> RacerObs; //AI objects*/
    public static List<RacePosition> RacePositions = new List<RacePosition>(); //All racers, used to record race positions
    
    public static float RepeatRate = 0.1f;
    #endregion

    GameObject RacerParent;

    // Use this for initialization
    void Awake()
    {
        /* RacerObs = new List<GameObject>();
         RacerObs.Add(RaceManager.PlayerOb);

         RacePositions = new List<RacePosition>();

         AIRacers = GameObject.FindGameObjectsWithTag(RacerTag); //Find AI Objs*/

        RacerParent = GameObject.Find("Racers");

        /*for (int i = 0; i < RacerParent.transform.childCount - 1; i++)
        {
            Debug.Log(RacerParent.transform.GetChild(i).name);
        }*/

        AddToList();

        InvokeRepeating("CalcRacePositions", RepeatRate, RepeatRate);
    }

    void AddToList() //Add all racers to the list, including Player object at the end //Something not working here BJC
    {

        //RacePositions.Add(RaceManager.PlayerOb.GetComponent<RacePosition>());
        RacePositions.Add(GameObject.FindGameObjectWithTag("Player").GetComponent<RacePosition>());

        for (int i = 0; i < RacerParent.transform.childCount; i++)
        {
            if (RacerParent.transform.GetChild(i).tag == RacerTag)
            {
                RacePosition RP = RacerParent.transform.GetChild(i).GetComponent<RacePosition>();
                RacePositions.Add(RP);

                //Debug.Log(RacePositions[i].gameObject.name);
            }
        }

        //RaceManager.CalcRaceConVal(RacePositions.Count);

        /*for (int i = 0; i < AIRacers.Length; i++) //Collect all Racer Objects together.
                {
                    RacerObs.Add(AIRacers[i]);
                }

                for (int j = 0; j < RacerObs.Count; j++) //Add RacePosition Components 
                {
                    RacePositions.Add(RacerObs[j].GetComponent<RacePosition>());
                    //Debug.Log(RacePositions[i].gameObject.name);
                }*/
                
    }

    public static int RacerPos(GameObject Racer)
    {
        int Pos = 0;

        for (int i = 0; i < RacePositions.Count - 1; i++)
        {
            if (Racer == RacePositions[i].gameObject)
            {
                Pos = i;
                return Pos;
            }
        }

        return Pos;
    }

    /*public static void RemoveRacer(GameObject Racer) { //Remove from list for race conclusion
        for (int i = 0; i < RacerObs.Count - 1; i++)
        {
            if (RacerObs[i] == Racer) {
                RacerObs.RemoveAt(i);
            }
        }
    }*/
    
    public  void CalcRacePositions()  //SORT THE RACE POSITIONS
    {
        List<RacePosition> RaceOrder;

        RaceOrder = RacePositions.OrderByDescending(x => x.ProgressVal()).ToList();

        /* RUN ANOTHER COMPARISON*/
        //RaceOrder.Reverse(0, RaceOrder.Count);

        RacePositions = RaceOrder;

    }

    public static void StopAllRacers()
    {
        for (int i = 0; i < RacePositions.Count; i++)
        {
            if (RacePositions[i].tag == RacerTag)
            {
                RacePositions[i].GetComponent<AISpeedControl>().StopCharacter();
            }
        }

        AltoRaceController.AllowMovement = false;
    }
}

/*public class RacerCompare : IComparer<int>
{
    public int Compare(int x, int y)
    {
        if (x == 0)
        {
            if (y == 0)
            {
                // If x is null and y is null, they're
                // equal. 
                return 0;
            }
            else
            {
                // If x is null and y is not null, y
                // is greater. 
                return -1;
            }
        }
        else
        {
            // If x is not null...
            //
            if (y == 0)
            // ...and y is null, x is greater.
            {
                return 1;
            }
            else
            {
                // ...and y is not null, compare the 
                // lengths of the two strings.
                //
                int retval = x.CompareTo(y);

                if (retval != 0)
                {
                    // If the strings are not of equal length,
                    // the longer string is greater.
                    //
                    return retval;
                }
                else
                {
                    // If the strings are of equal length,
                    // sort them with ordinary string comparison.
                    //
                    return x.CompareTo(y);
                }
            }
        }
    }
}*/
