﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetachShield : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Missile")
        {
            DetachNow();
        }
    }

    public void DetachNow()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.None;
        rb.useGravity = true;

        gameObject.transform.SetParent(null);

        Destroy(gameObject, 10.0f);
    }
}