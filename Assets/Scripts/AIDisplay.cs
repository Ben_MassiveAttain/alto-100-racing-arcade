﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AIDisplay : MonoBehaviour {

    public string DispName;

    public Text PositionText;

    public Text NameText;
    
    RacePosition RP;

    private void Start()
    {
        NameText.text = DispName;

        RP = GetComponent<RacePosition>();
    }

    private void Update()
    {
        DispPos();
    }

    void DispPos()
    {
     //PositionText.text = RP.RacePosVal.ToString() + ", " + WayPointVal.ToString();
     PositionText.text = ScoreManager.PosTextVal(RP.RacePosVal);

    }
}
