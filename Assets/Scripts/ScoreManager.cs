﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    static public GameObject TextOb;

    private void Awake()
    {
        TextOb = GameObject.Find("Text Disp");
        DisableOb(false);
    }
    
    public static void NewTextOb(string Name, int Position)
    {
        Transform TextParent = TextOb.transform.parent;

        GameObject NewTextOb = Instantiate(TextOb, TextParent.transform.position, TextParent.transform.rotation, TextParent) as GameObject;

        NewTextOb.SetActive(true);

        NewTextOb.GetComponent<Text>().text = (PosTextVal(Position) + " - Racer: " + Name);
    }

    public static string PosTextVal(int Position)
    {
        string PosVal;

        switch (Position)
        {
            case 1:
                PosVal = "st";
                break;

            case 2:
                PosVal = "nd";
                break;

            case 3:
                PosVal = "rd";
                break;

            default:
                PosVal = "th";
                break;
        }

        return (Position + PosVal);
    }

    public static void DisableOb(bool State) {
        TextOb.SetActive(State);
    }
}
