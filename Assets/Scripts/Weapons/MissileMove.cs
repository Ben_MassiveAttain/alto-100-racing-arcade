﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileMove : MonoBehaviour {

    public float Speed = 5.0f;

    public GameObject TargetOb;

    public static int DamageValue = 50;

    public GameObject ExplosionOb;

    private void Start()
    {
        Invoke("DestroyThis", 10.0f);
    }

    void Update()
    {
        if (TargetOb != null)
        {
            float step = Speed * Time.deltaTime;

            transform.position = Vector3.MoveTowards(transform.position, TargetOb.transform.position, step);

            transform.LookAt(TargetOb.transform.position);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.gameObject.layer == 10) {
            collision.gameObject.GetComponent<AISpeedControl>().DownSpeed();
        }

        if (collision.transform.gameObject.layer == 11)
        {
            collision.transform.GetComponent<DetachShield>().DetachNow();
        }

        DestroyThis();
    }

    void DestroyThis()
    {
        GameObject ExOb = Instantiate(ExplosionOb, transform.position, transform.rotation) as GameObject;
        Destroy(ExOb, 1.0f);

        Destroy(gameObject);
    }
}
