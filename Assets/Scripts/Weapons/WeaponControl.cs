﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponControl : MonoBehaviour {

    public bool HasWeapon = false;

    public GameObject WeaponOb;

    public Transform RadarPoint;

    public Transform InstPoint;

    //GameObject TargetOb;

    public LayerMask LM;

    public float RepeatRate = 0.25f;

    public float ShootDelay = 2.0f;

    public float RadVal = 2.0f;

    float DelayTime;

    private void Start()
    {
        DelayTime = Time.time + ShootDelay;

        /*if (tag == "Player")
        {

        }*/
    }

    // Update is called once per frame
    void Update () {

        //if (Time.time > DelayTime || Input.GetButtonDown("Fire1"))
        
        if (Time.time > DelayTime)
        {

            /*//if (Input.GetButtonDown("Fire1"))
            //{
                if (gameObject.tag == "Player")
                {
                    RadarCatch();
                    DelayTime = Time.time + ShootDelay;
                }
            //}

            if (gameObject.tag == "Enemy")
            {
                RadarCatch();
                DelayTime = Time.time + ShootDelay;
            }*/

            RadarCatch();
            DelayTime = Time.time + ShootDelay;
        }
	}

    void RadarCatch()
    {
        RaycastHit raycastHit;
        
        if (Physics.SphereCast(RadarPoint.transform.position, RadVal, RadarPoint.transform.forward, out raycastHit, 500.0f, LM)) {
            //Vector3 Dir = raycastHit.transform.position - RadarPoint.transform.position;

            if (HasWeapon)
            {
                FireWeapon(raycastHit.transform.gameObject);
               //Debug.DrawRay(RadarPoint.transform.position, RadarPoint.transform.forward, Color.green, 0.1f);
            }
        }
    }

    public void AssignWeapOn(GameObject Weapon) {
        DelayTime = Time.time + ShootDelay;

        WeaponOb = Weapon;
        HasWeapon = true;
    }

    void FireWeapon(GameObject TargOb)
    {
        if (HasWeapon) {
            GameObject Missile = Instantiate(WeaponOb, InstPoint.transform.position, InstPoint.transform.rotation) as GameObject;

            Missile.GetComponent<MissileMove>().TargetOb = TargOb;

            /*WeaponOb = null;
            HasWeapon = false;*/
        }
    }
}


