﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Alto;

public class PlayerInterface : MonoBehaviour {

    public Text LapText;
    //static Text WPText;
    public Text RPText;
    public Text SpeedText;
    public static Text StageText;
    
    public RacePosition RP;
    
    int LapVal;
    //int WPVal;
    //int HealthVal;
    float SpeedVal;

    static GameObject RetImage;

    public Rigidbody RB;

    //public AltoRaceController RaceCont;

    WeaponControl PWeaponCon;

    // Use this for initialization
    void Start () {
        LapText = transform.Find("Lap Text").GetComponent<Text>();
        //WPText = transform.Find("Waypoint Text").GetComponent<Text>();
        SpeedText = transform.Find("Speed Text").GetComponent<Text>();

        StageText = transform.Find("Stage Text").GetComponent<Text>();

        RetImage = transform.Find("Target Ret").gameObject;

        RetImage.SetActive(false);

        PWeaponCon = GetComponentInParent<WeaponControl>();

        //RP = GetComponentInParent<RacePosition>();
    }

    private void Update()
    {
        LapVal = RaceManager.RP.LapCount;
        //WPVal = RaceManager.RP.WayPointVal;

        //HealthVal = RaceManager.HPCont.CurrentHealthVal;
        SpeedVal = RB.velocity.magnitude;

        RetImage.SetActive(PWeaponCon.HasWeapon);

        LapText.text = "LAP: " + LapVal;
        //WPText.text = "LAST WAYPOINT: " + WPVal;
        //HealthText.text = "HP: " + HealthVal;

        RPText.text = "Race Postion: " + RP.RacePosVal.ToString();
        SpeedText.text = "SPEED: " + (int)SpeedVal;
    }

    /*public static void TargetRacer(bool Enabled) {
        RetImage.enabled = Enabled;
    }*/

    public static void DispStageText(string Val)
    {
        StageText.gameObject.SetActive(true);
        StageText.text = Val;
    }

    public void HideText()
    {
        StageText.gameObject.SetActive(false);
    }

    public void StartCountDown(float Val)
    {
        StartCoroutine(CountDown(Val));
    }

    public IEnumerator CountDown(float Val)
    {
        float CountDownVal = Val;

        while (CountDownVal > -1)
        {
            yield return new WaitForSeconds(1.0f);

            StageText.text = CountDownVal + "...";

            CountDownVal -= 1.0f;
        }

        ToggleStageText(false);
    }

    void ToggleStageText(bool Val)
    {
        StageText.enabled = Val;
    }
}
