﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class MainMenu : MonoBehaviour {

    public void LoadNew()
    {
        SceneController.LoadNextScene("Straight Track Test");
    }

    public void CloseApplication()
    {
        Application.Quit();
    }
}
