﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alto;

public class RaceFinishLine : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            other.GetComponent<AISpeedControl>().StopCharacter();
        }

        if (other.tag == "Player")
        {
            RaceManager.PlayerOb.GetComponent<AltoRaceController>().StopAlto();
            RaceManager.PlayerOb.GetComponent<WeaponControl>().enabled = false;
        }
    }

}
