﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadNextLevel : MonoBehaviour {

    public float SceneTimeChange = 5.0f;

    public int SceneNum = 0;

    public void LoadNext()
    {
        StartCoroutine(WaitLevelChange());
    }

    IEnumerator WaitLevelChange()
    {
        yield return new WaitForSeconds(SceneTimeChange);

        SceneController.LoadNextScene(SceneNum);
    }
}
