﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIAnimSpin : SpinAnim {

    AISpeedControl SpeedCont;

    ParticleSystem Particles;

    private void Start()
    {
        SpeedCont = GetComponent<AISpeedControl>();

        Particles = PivOb.GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        base.RotSpeed = SpeedCont.CurrentSpeed;

        var main = Particles.main;

        main.simulationSpeed = Mathf.RoundToInt(base.RotSpeed);
    }
}
