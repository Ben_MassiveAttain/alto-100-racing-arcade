﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class LeftHandControl : MonoBehaviour {

    public GameObject Hoverboard;

    public GameObject Head;

    public Rigidbody HoverboardBody;

    public float ThrustAmount;

    public bool AllowThumbSteer = false;


    public GameObject MissleLauncher;

    private SteamVR_TrackedController controller;

    private SteamVR_TrackedObject trackedObject;

    private SteamVR_Controller.Device device;


    [SerializeField]

    private float thrustMultipler = 14f;



    [SerializeField]

    private float maxVelocity = 1f;



    private void OnEnable()

    {

        controller = GetComponent<SteamVR_TrackedController>();
        trackedObject = GetComponent<SteamVR_TrackedObject>();
     //   controller.TriggerClicked += ShootMissle;
    }

    public void ShootMissle(object sender, ClickedEventArgs e)
    {
        MissleLauncher.SendMessage("ShootMissle");
    }


    private void FixedUpdate()

    {

       // print(OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick));
       // print(OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick));

        var thrust = controller.controllerState.rAxis1.x;

        device = SteamVR_Controller.Input((int)trackedObject.index);

      //  print(device);

        if (AllowThumbSteer)
        {

        
        if (device.GetAxis().x != 0 || device.GetAxis().y != 0)
        {

            // Debug.Log(device.GetAxis().x + " " + device.GetAxis().y);

            Vector2 ControlDir = new Vector2(device.GetAxis().x, device.GetAxis().y);

            // Hoverboard.SendMessage("ControllerMove", ControlDir);
            //  HoverboardBody.AddForce(Hoverboard.transform.forward * (ControlDir.y *2000));
            //  HoverboardBody.AddForce(Hoverboard.transform.right * (ControlDir.x * 2000));
            Vector3 CurRot = Hoverboard.transform.eulerAngles;
            CurRot.y += ControlDir.x;
            Hoverboard.transform.eulerAngles = CurRot;

            HoverboardBody.AddForce(Head.transform.forward * (ControlDir.y * 2000));
            HoverboardBody.AddForce(Head.transform.right * (ControlDir.x * 2000));

        }
        }

        if (thrust > 0.1)

        {

            //  var forceVector = transform.forward * thrust * thrustMultipler;
            var forceVector = transform.forward * thrust;
            print(thrust);
            // Hoverboard.SendMessage("ControllerFly", thrust);
            HoverboardBody.AddRelativeForce(new Vector3(0, thrust, 0) * ThrustAmount);
            SteamVR_Controller.Input((int)controller.controllerIndex).TriggerHapticPulse((ushort)(200f * thrust));

        }

    }

}

