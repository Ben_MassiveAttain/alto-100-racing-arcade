﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {
    
    public static void LoadNextScene(string SceneVal)
    {
        SceneManager.LoadScene(SceneVal);
    }

    public static void LoadNextScene(int SceneVal)
    {
        SceneManager.LoadScene(SceneVal);
    }
}
