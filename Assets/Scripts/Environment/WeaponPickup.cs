﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickup : PickUp {

    public GameObject Weapon;

    public override void AITriggered()
    {
        RecOb.GetComponent<WeaponControl>().AssignWeapOn(Weapon);
    }

    public override void PlayerTriggered()
    {
        RecOb.GetComponent<WeaponControl>().AssignWeapOn(Weapon);
    }
}
