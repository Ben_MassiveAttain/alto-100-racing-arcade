﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratePickUps : MonoBehaviour {
    
    public GameObject[] PickUpsOptions;
    
    public int Freq = 4;

    WaypointGenerator WPGen;

    public List<GameObject> PickUps = new List<GameObject>();

    Transform ParentPoint;
    
    public void PlaceHPPickups()
    {
        ParentPoint = GameObject.Find("Pick Ups").transform;

        ClearPickUps();

        WPGen = GetComponent<WaypointGenerator>();

        for (int i = 0; i < WPGen.WayPoints.Count; i++)
        {
            if (i % Freq == 0) {
                int PickUpID = Random.Range(0, PickUpsOptions.Length);
                GameObject NewPickUp =  Instantiate(PickUpsOptions[PickUpID], WPGen.WayPoints[i].transform.position, WPGen.WayPoints[i].transform.rotation) as GameObject;

                NewPickUp.transform.SetParent(ParentPoint);

                PickUps.Add(NewPickUp);
            }
        }
    }

    public void ClearPickUps() {
        for (int i = 0; i < PickUps.Count; i++)
        {
            DestroyImmediate(PickUps[i]);
        }

        PickUps.Clear();
    }
}
