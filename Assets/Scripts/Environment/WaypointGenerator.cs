﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointGenerator : MonoBehaviour {

    List<Vector3> TransVerts = new List<Vector3>();

    public List<GameObject> WayPoints = new List<GameObject>();

    public int WPCount;

    public static int CountVal;
    
    public GameObject WPob;

    public float TurnDistThresh = 5.0f;

    private void Start()
    {
        CountVal = WPCount;
        ReturnWPPos(0);
    }

    public void CalcAverageDist()
    {
        StorePositions();

        float OverallDist = 0.0f;
        int CountVal = TransVerts.Count;

        for (int i = 0; i < CountVal; i++)
        {
            if (i + 1 < CountVal)
            {
                float CurrDist = Vector3.Distance(TransVerts[i], TransVerts[i + 1]);
                OverallDist += CurrDist;
            }
        }

        float Dist = OverallDist / CountVal;

        //Debug.Log("Overall Distance = " + Dist);
    }
    
    public void InstantiateWPs()
    {
        RemoveWPs();

        StorePositions();

        for (int i = 0; i < TransVerts.Count - 1; i++)
        {

            int NextVal = i + 1;

            if (NextVal > TransVerts.Count)
            {
                NextVal = 0;
            }

            float Dist = Vector3.Distance(TransVerts[i], TransVerts[i + 1]);

            GameObject NewWP = Instantiate(WPob, transform.InverseTransformPoint(TransVerts[i]), WPob.transform.rotation) as GameObject;

            WayPoints.Add(NewWP);

            NewWP.transform.SetParent(transform);

            WayPoints[i].GetComponent<RaceWayPoint>().WPid = i;

        }

        for (int j = 0; j <= WayPoints.Count; j++)
        {
            if (j < WayPoints.Count - 1)
            {
                LookAtNextWP(WayPoints[j], WayPoints[j + 1].transform.position);
            }
            else if (j > WayPoints.Count) {

                int LastWP = WayPoints.Count - 1;

                LookAtNextWP(WayPoints[LastWP], WayPoints[0].transform.position);

                WayPoints[LastWP].GetComponent<RaceWayPoint>().AssignFinishWP();

            }
        }

        WPCount = WayPoints.Count;
        //WayPointObs = WayPoints.ToArray();
    }

    void LookAtNextWP(GameObject Ob, Vector3 Pos) {
        Ob.transform.rotation = new Quaternion(0.0f, 0.0f, Ob.transform.rotation.z, Ob.transform.rotation.w);

        Ob.transform.LookAt(Pos);

        Ob.transform.SetParent(transform);
    }

    public void RemoveWPs()
    {
        for (int i = 0; i < WayPoints.Count; i++)
        {
            DestroyImmediate(WayPoints[i]);
        }

        ClearPos();

        WayPoints.Clear();
    }
    
    public void StorePositions()
    {
        //ClearPos();

        Mesh VertMesh = GetComponent<MeshFilter>().sharedMesh;

        for (int i = 0; i < VertMesh.vertexCount; i++)
        {

            if ((i % 4) == 0)
            {
                float X = VertMesh.vertices[i].x - (VertMesh.vertices[i].x - VertMesh.vertices[i + 1].x) / 2;
                float Y = VertMesh.vertices[i].y - (VertMesh.vertices[i].y - VertMesh.vertices[i + 1].y) / 2;
                float Z = VertMesh.vertices[i].z - (VertMesh.vertices[i].z - VertMesh.vertices[i + 1].z) / 2;

                Vector3 Newtrans = new Vector3(X, Y, Z);

                TransVerts.Add(Newtrans);
            }
        }
    }

    public void ClearPos()
    {
        TransVerts.Clear();
    }

    public Vector3 ReturnWPPos(int Val) {
        //Debug.Log("ID: " + Val + ", " + WayPoints[Val].name);
        Vector3 NextWPPos = WayPoints[Val].transform.position;
        return NextWPPos;
    }

    public static int WPCountCheck() {
        return CountVal;
    }
}

