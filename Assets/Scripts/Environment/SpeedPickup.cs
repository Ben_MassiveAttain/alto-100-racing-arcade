﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alto;

public class SpeedPickup : PickUp {

    public bool IncDecToggle = true;

    public GameObject EmitterOb;

    public override void AITriggered()
    {
        if (IncDecToggle)
        {
            RecOb.GetComponent<AISpeedControl>().SpeedIncValChange(1);
        }
        else
        {
            RecOb.GetComponent<AISpeedControl>().SpeedIncValChange(-1);
        }

        EmitterObSpawn();

    }

    public override void PlayerTriggered()
    {
        if (IncDecToggle)
        {
            RecOb.GetComponent<AltoRaceController>().SpeedIncCont(1);
        }
        else
        {
            RecOb.GetComponent<AltoRaceController>().SpeedIncCont(-1);
        }

        EmitterObSpawn();
    }

    void EmitterObSpawn()
    {
        GameObject EmOb =  Instantiate(EmitterOb, transform.position, EmitterOb.transform.rotation) as GameObject;
        Destroy(EmOb, 1.0f);
    }
}
