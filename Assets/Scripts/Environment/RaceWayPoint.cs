﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceWayPoint : MonoBehaviour {

   public bool FinalWP = false; //If this is the finishing line of the racetrack

   public int WPid = 0;
   public int NextWP;

    private void Start()
    {
        CheckNextWP();
    }

    public void CheckNextWP()
    {
        NextWP = WPid + 1;

        if (FinalWP)
        {
            NextWP = 0;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 10)
        {
            //Debug.Log("HAPPENED");

            //RP.AddVal();
            RacePosition RP = other.GetComponent<RacePosition>();
            RP.AddVal(WPid);
            
            if (other.tag == "Enemy")
            {
                //Debug.Log("HAPPENED: " + gameObject.name);

                other.GetComponent<AIMovement>().MoveNavAgent(NextWP);
            }

            if (FinalWP)
            {
                RP.CalcLapVal();

                RaceManager.ConcludeRace();
            }
        }
    }

    public void AssignFinishWP() {
        FinalWP = true;
    }
}
