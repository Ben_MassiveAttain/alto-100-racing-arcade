﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour {

    [SerializeField]
    public GameObject RecOb;
    Vector3 AddPos = new Vector3(0.0f, 2.5f, 0.0f);

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 10) {
            RecOb = other.gameObject;

            if (other.tag == "Player")
            {
                PlayerTriggered();
            }

            if (other.tag == "Enemy")
            {
                AITriggered();
            }

            Invoke("ReEnableOb", RaceManager.PickUpTime);

            gameObject.SetActive(false);
        }
    }

    virtual public void PlayerTriggered()
    {
        Debug.Log("Player Triggered: " + gameObject.name);
    }

    virtual public void AITriggered()
    {
        Debug.Log("AI Triggered: " + gameObject.name);
    }

    void ReEnableOb() {
        gameObject.SetActive(true);
    }
    
}
