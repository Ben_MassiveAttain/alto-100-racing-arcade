﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alto;

public class PlayerSpinAnim : SpinAnim {

    AltoRaceController AltoCon;

    public AudioSource EngineSound;

    private void Start()
    {
        AltoCon = GetComponent<AltoRaceController>();
    }

    // Update is called once per frame
    void Update () {
        base.RotSpeed = AltoCon.userVelocity;

        EngineSound.pitch = RotSpeed * 0.25f;
    }
}
