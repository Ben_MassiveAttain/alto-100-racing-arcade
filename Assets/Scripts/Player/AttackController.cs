﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackController : MonoBehaviour {

    public GameObject MissileOb;

    public Transform RadarPoint;

    bool HasTarget = false;

	// Update is called once per frame
	void Update () {
        
        if (Input.GetButtonDown("Fire1")) {
            if (HasTarget) {
                Attack();
            }
        }
	}

    public void Attack() { //
        Debug.Log("Attack Occurred");
    }

}
