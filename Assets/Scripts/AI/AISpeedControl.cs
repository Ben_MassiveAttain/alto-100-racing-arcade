﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AISpeedControl : SpeedControl {

    NavMeshAgent ThisNav;

    /*public static float NavSpeed = 1.0f;
    public static float NavAngSpeed = 1.0f;
    public static float NavAcc = 1.0f;*/

    public static float NavSpeed = 100.0f;
    public static float NavAngSpeed = 55.0f;
    public static float NavAcc = 80.0f;

    public float SpeedAddVal = 10.0f;
    public int IncSpeedVal = 0;

    float RandSpeedVal;

    // Use this for initialization
    void Start () {
        ThisNav = GetComponent<NavMeshAgent>();

        //NewSpeed = NavSpeed + (NavSpeed * MaxIncrease);

        InitSpeed();
    }

    void InitSpeed()
    {
        RandSpeedVal = Random.Range(-10, 10);

        ThisNav.speed = NavSpeed + RandSpeedVal;
        ThisNav.angularSpeed = NavAngSpeed;
        ThisNav.acceleration = NavAcc;
    }

    private void Update()
    {
        CurrentSpeed = ThisNav.velocity.magnitude;
    }

    public void SpeedIncValChange(int Val)
    {
        IncSpeedVal += Val;

        if (IncSpeedVal < 0)
        {
            IncSpeedVal = 0;
        }

        ChangeSpeed();
    }

    public void DownSpeed()
    {
        IncSpeedVal = 0;
        ChangeSpeed();
    }

    void ChangeSpeed()
    {
        ThisNav.speed += (IncSpeedVal * SpeedAddVal);
    }

    public void StopCharacter()
    {
        ThisNav.isStopped = true;
    }
}
