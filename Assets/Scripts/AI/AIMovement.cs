﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIMovement : MonoBehaviour {

    NavMeshAgent ThisNav;

    static float VariationVal = 15.0f;

    Vector3 TargetPos;

    //public int WPCount = 0;

    public WaypointGenerator WPGen;

    Rigidbody RB;
    RacePosition RP;


    // Use this for initialization
    void Start () {
        ThisNav = GetComponent<NavMeshAgent>();
        RB = GetComponent<Rigidbody>();
        
        //RP = GetComponent<RacePosition>();
       //WPGen = GameObject.Find("Race Controller").GetComponent<WaypointGenerator>();
    }

    public void MoveNavAgent(int PosID)
    {
        //Debug.Log("HAPPENED: " + PosID);

        float VariPosVal = Random.Range(-VariationVal, VariationVal);

        Vector3 WPPos = WPGen.ReturnWPPos(PosID);

        Vector3 VarPos = new Vector3(WPPos.x + VariPosVal, WPPos.y, WPPos.z + VariPosVal);

        if (!ThisNav.hasPath)
        {
            ThisNav.SetDestination(VarPos);
        }
        else
        {
            ThisNav.ResetPath();

            ThisNav.SetDestination(VarPos);
        }

        //Debug.Log(ThisNav.pathStatus.ToString());
        //ThisNav.ResetPath();

    }

    public void RacerDeathReset()
    {
        StartCoroutine(HaltRacer());
    }

    IEnumerator HaltRacer()
    {
        ThisNav.isStopped = true;
        yield return new WaitForSeconds(5.0f);
        ThisNav.isStopped = false;
    }

    void PosState(bool Stopped)
    {
        if (Stopped)
        {
            RB.constraints = RigidbodyConstraints.FreezeAll;
            ThisNav.isStopped = true;
        }
        else
        {
            RB.constraints = RigidbodyConstraints.None;
            ThisNav.isStopped = false;
        }
    }
}
