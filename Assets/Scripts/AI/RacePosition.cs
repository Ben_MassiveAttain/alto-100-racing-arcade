﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacePosition : MonoBehaviour {
    public float DistCovered = 0.0f;

    public int WayPointVal = 0; //The last waypoint this racer crossed;

    public int LapCount = 0;

    public int RacePosVal;

    public WaypointGenerator WPGen;

    //List<Vector3> DistTrans = new List<Vector3>();
    Vector3 PrevPos;

    private void Start()
    {

        PrevPos = transform.position;

        //RacePositionManager.AddRacertoPositions(this);

        InvokeRepeating("CheckPos", RacePositionManager.RepeatRate, RacePositionManager.RepeatRate);

    }

    public void AddVal(int WPVal) { //Called by WP trigger

        WayPointVal = WPVal;
        //DistTrans.Add(transform.position);

        CalcOverallDist();
    }

    public void CheckPos()
    {
        RacePosVal = RacePositionManager.RacerPos(gameObject) + 1;
    }
    
    void CalcOverallDist() {
        
        float DisttoLastWP = Vector3.Distance(transform.position, PrevPos);

        PrevPos = transform.position;

        DistCovered += DisttoLastWP;
    }

    public void CalcLapVal() {
        LapCount++;
        
        if (LapCount == RaceManager.MaxLapCount)
        {
            RaceManager.CheckConc();

            CancelInvoke();
        }
    }

    public int ProgressVal() {
        int progVal;

        if (LapCount <= 0)
        {
            progVal = WayPointVal;
        }
        else
        {
            progVal = (LapCount * RaceManager.WPCount) + WayPointVal;
        }

        return WayPointVal;
    }
}
