﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOnKey : MonoBehaviour {

    public float viewAngle = 5f;
    public float smoothing = 0.5f;

    IEnumerator RotateMe(Vector3 byAngles, float inTime)
    {
        //var fromAngle = transform.rotation;
        //var toAngle = Quaternion.Euler(transform.eulerAngles + byAngles);
        //for(var t = 0f; t < 1; t += Time.deltaTime/inTime)
        //{
        //    transform.rotation = Quaternion.Slerp(fromAngle, toAngle, t);
        //    yield return null;
        //}
        transform.Rotate(byAngles, Space.Self);
        yield return null;
    }
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown("f"))
        {
            StartCoroutine(RotateMe(Vector3.left * viewAngle, smoothing));
        }
        if (Input.GetKeyUp("f"))
        {
            StartCoroutine(RotateMe(Vector3.left * -viewAngle, smoothing));
        }
        if (Input.GetKeyDown("g"))
        {
            StartCoroutine(RotateMe(Vector3.right * viewAngle, smoothing));
        }
        if (Input.GetKeyUp("g"))
        {
            StartCoroutine(RotateMe(Vector3.right * -viewAngle, smoothing));
        }
    }
}
