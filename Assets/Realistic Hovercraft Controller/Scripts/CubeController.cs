﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeController : MonoBehaviour {

    public float speed;
    public float tilt;

    private Rigidbody rb;
    private Vector3 lastVelocity;

    void Start () {
        rb = GetComponent<Rigidbody>();
        lastVelocity = new Vector3(0, 0, 0);
	}
	
	
	void FixedUpdate () {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        Vector3 accel = (rb.velocity - lastVelocity) / Time.fixedDeltaTime;
        lastVelocity = rb.velocity;
        
        transform.Rotate(Quaternion.AngleAxis(90,Vector3.up) * accel * tilt, Space.World); // multiply by the vertical axis and rotate 90degrees
        //Vector3 lastVelocity = rb.velocity;

        rb.AddForce(movement * speed);
    }
}
