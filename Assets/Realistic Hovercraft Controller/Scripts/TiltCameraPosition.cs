﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(HoverboardController))]
public class TiltCameraPosition : MonoBehaviour {

    // these are used for setting up GetComponent
    public GameObject otherGameObject;
    private Alto.AltoController altoController;

    public Vector3 planeAccel;
    public float angleTilt;
    public Vector3 accelNormalized;
    public Vector3 rotationAxis;
    public Quaternion target;

    private void Awake()
    {
        altoController = otherGameObject.GetComponent<Alto.AltoController>();
    }

    private void FixedUpdate()
    {
        /*
        planeAccel = hoverboardController.accelXZ; // get the acceleration on the horizontal plane
        angleTilt = Mathf.Atan2(planeAccel.magnitude, 9.8f) * Mathf.Rad2Deg; // the angle tilt formula acceleration and gravity
        accelNormalized = planeAccel.normalized; // want just the direction of the acceleration
        rotationAxis = Quaternion.AngleAxis(90, Vector3.up) * accelNormalized * angleTilt; //create the vector 90 degs from the accel vector, to rotate about
        target = Quaternion.Euler(rotationAxis); // create the target quat to
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.time); // Time.time moves the camera in that frame, and Time.fixedDeltaTime moves it incrementally over time.
    */
         }
}
