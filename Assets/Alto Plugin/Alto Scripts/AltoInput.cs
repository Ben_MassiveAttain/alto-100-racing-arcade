﻿using UnityEngine;
using AltoDriver;

namespace Alto
{
public class AltoInput : MonoBehaviour
{

    // Singelton pattern
    static AltoInput instance;
    static AltoInput Instance
    {
        get
        {
            if (instance == null)
            {

                instance = FindObjectOfType<AltoInput>();
            }
            return instance;
        }
    }

    static AltoConfig altoConfig;
    static AltoConfig AltoConfigInstance
        {
            get
            {
                if(altoConfig == null)
                {
                    altoConfig = FindObjectOfType<AltoConfig>();
                }
                return altoConfig;
            }
        }

    public static float GetDirection()
    {

        return Instance.GetDirectionInternal();
    }

    public static float GetMagnitudeNormalised()
    {

        return Instance.GetMagnitudeNormalisedInternal();
    }

    public static bool GetOn()
    {
        return Instance.GetOnInternal();
    }

    public static bool IsPresent()
    {
        return Instance.IsPresentInternal();
    }

    public static void SendHapticCommand(string state)
    {
        Instance.SendHapticCommandInternal(state, 9);
    }

    public static void SendHapticCommand(string state, int intensity)
    {
        Instance.SendHapticCommandInternal(state, intensity);
    }

    public static string CreateNewHapticState(string name, int motor, int min, int max, bool continuous)
    {
        return Instance.CreateNewHapticStateInternal(name, motor, min, max, continuous);
    }

    private Alto_Driver driver;

    void Start()
    {
        string vID = AltoConfigInstance.GetSetting("Alto", "VID");
        string pID = AltoConfigInstance.GetSetting("Alto", "PID");
        driver = new Alto_Driver(vID, pID);

        //for moving across the ground
        driver.CreateHapticState("Ground", Alto_Driver.MotorName.MotorOne, 0, 150, Alto_Driver.MotorActivationType.Continuous);
        //for when in the air
        driver.CreateHapticState("Hover", Alto_Driver.MotorName.MotorTwo, 0, 100, Alto_Driver.MotorActivationType.Continuous);
        //for when on water
        driver.CreateHapticState("Water", Alto_Driver.MotorName.MotorOne, 0, 200, Alto_Driver.MotorActivationType.Continuous);
        //for when collided with object or land with jetpack
        driver.CreateHapticState("Impact", Alto_Driver.MotorName.MotorTwo, 150, 1000, Alto_Driver.MotorActivationType.Continuous);
        //for when Alto represents a vehicle (steam engine style)
        driver.CreateHapticState("Vehicle", Alto_Driver.MotorName.MotorOne, 0, 200, Alto_Driver.MotorActivationType.Continuous);
        //for when stopping Alto haptics
        driver.CreateHapticState("StopMotor1", Alto_Driver.MotorName.MotorOne, 0, 0, Alto_Driver.MotorActivationType.Discrete);
        driver.CreateHapticState("StopMotor2", Alto_Driver.MotorName.MotorTwo, 0, 0, Alto_Driver.MotorActivationType.Discrete);
    }

    void OnApplicationQuit()
    {
        driver.Shutdown();
    }

    private void OnLevelWasLoaded(int level)
    {
        driver.resetDriver();
    }

    private float GetDirectionInternal()
    {
        return driver.data.direction;
    }

    private float GetMagnitudeNormalisedInternal()
    {
        return Mathf.Clamp(driver.data.magnitude / 100f, 0, 1);
    }

    private bool GetOnInternal()
    {
        return driver.data.on;
    }

    bool IsPresentInternal()
    {
        return driver.data.valid;
    }

    private void SendHapticCommandInternal(string state, int intensity)
    {
        driver.sendHapticCommand(state, intensity);
    }

    private string CreateNewHapticStateInternal(string name, int motor, int min, int max, bool continuous)
    {
        Alto_Driver.MotorName motorChoice;
        if (motor > 1)
        {
            motorChoice = Alto_Driver.MotorName.MotorTwo;
        }
        else
        {
            motorChoice = Alto_Driver.MotorName.MotorOne;
        }
        Alto_Driver.MotorActivationType motorActivationChoice;
        if (continuous)
        {
            motorActivationChoice = Alto_Driver.MotorActivationType.Continuous;
        }
        else
        {
            motorActivationChoice = Alto_Driver.MotorActivationType.Discrete;
        }
        driver.CreateHapticState(name, motorChoice, min, max, motorActivationChoice);
        return name;
    }
    }
}
