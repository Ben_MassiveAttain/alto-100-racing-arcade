﻿using UnityEngine;
using AltoDriver;
using System.Collections.Generic;
using UnityEngine.XR;
using System.Collections;

namespace Alto
{
    [RequireComponent(typeof(Rigidbody))]
    public class AltoController : MonoBehaviour
    {
        [Tooltip("Mulitplies normalised Magnitude Alto Input")]
        public float step = 10f;
        private Rigidbody rigidBody;
        private Collider colliderComponent;
        private Vector3 boardExtents;

        private Vector3 lastVelocity;

        //controller for jetpack, use trigger
        [Tooltip("One controller to control the jetpack goes here")]
        public SteamVR_TrackedController jetPackControl;

        [Tooltip("The puck 3D model")]
        public GameObject AltoPuck;
        [Tooltip("Speed Alto moves based on the input from Alto itself")]
        public float AltoSpeed;
        [Tooltip("The adjustment the player makes when using the touchpad for speed")]
        public float UserSpeedIncremenet;
        [Tooltip("Amount of force applied when jetpack control activated")]
        public float AltoHoverForce;
        private bool landing = false;

        private int userVelocity = 0; //used for haptics
        private float userSpeed = 0.0f; //used for user controlling their speed
        [Tooltip("Will be given by config file in future - angle correction for direction Alto is facing")]
        public float calibDirection;
        [Tooltip("Will be given by config file in future - direction to apply angle correction to Alto")]
        public Vector3 calibVector;
        [Tooltip("For calibrating in game - not implemented yet")]
        public bool isCalibrating = false;

        public GameObject rightController;
        public GameObject gunObject;
        public GameObject guiArmObject;
        public bool armed = false;

        [System.Serializable]
        public struct CustomHapticState
        {
            public string name;
            public int motor;
            public int min;
            public int max;
            public bool continuous;
        }
        [HideInInspector]
        public CustomHapticState customHapticState;
        [Tooltip("This is for custom Alto Haptics - the basic haptics can be seen in AltoInput")]
        public List<CustomHapticState> customHapticStates;
        //this is where the names are stored automatically for custom haptics you create, so that you can call them in script
        [HideInInspector]
        public List<string> hapticStateNames;
        private float waterCounter = 0.0f;

        private bool checkingInactivity = false;
        private bool altoInactive = false;
        [HideInInspector] //used by gamelogic for text info
        public bool hovering = false;
        private bool impacting = false;

        private float previousYPos = 0.0f;

        public enum AltoBoardState
        {
            Static, //does not move
            Dynamic, //can move on X and Z axis only
            DynamicJetPack //can move on X, Y and Z axis (can use jetpack controls)
        }
        [Tooltip("State Alto is in during runtime - to control how player can use Alto")]
        public AltoBoardState altoState;

        //getters for variables
        private Vector3 altoDirection = Vector3.zero;
        public Vector3 GetAltoDirection
        {
            get { return altoDirection; }
        }

        private float altoMagnitude = 0.0f;
        public float GetAltoMagnitude
        {
            get { return altoMagnitude; }
        }

        void Awake()
        {
            InitializeController();
        }

        void Start()
        {
            InitializeController();
        }

        private void InitializeController()
        {
            rigidBody = GetComponent<Rigidbody>();
            colliderComponent = GetComponent<Collider>();
            boardExtents = colliderComponent.bounds.extents;
            calibVector = Vector3.forward;
            rightController.SetActive(true);
            gunObject.SetActive(false);
            guiArmObject.GetComponent<Canvas>().enabled = false;
            hapticStateNames = new List<string>();
            //TODO: put this list in AltoInput and access from there
            hapticStateNames.Add("Ground");
            hapticStateNames.Add("Air");
            hapticStateNames.Add("Water");
            hapticStateNames.Add("LandA");
            hapticStateNames.Add("LandB");
            hapticStateNames.Add("Collide");
            foreach (CustomHapticState chs in customHapticStates)
            {
                string name = AltoInput.CreateNewHapticState(chs.name, chs.motor, chs.min, chs.max, chs.continuous);
                hapticStateNames.Add(name);
            }
        }

        ///Alto functions///
        private void ProcessAltoInput()
        {
            //get input values from Input class
            float altoInputDirection = AltoInput.GetDirection();
            float altoInputMagnitudeNorm = AltoInput.GetMagnitudeNormalised();
            altoDirection = (Quaternion.AngleAxis(altoInputDirection - calibDirection + 90, Vector3.up)) * calibVector;

            altoMagnitude = step * altoInputMagnitudeNorm;
            Vector3 PuckRot = Quaternion.AngleAxis(altoInputDirection + 90, Vector3.up) * Vector3.forward;
            AltoPuck.transform.eulerAngles = PuckRot * (altoInputMagnitudeNorm * 20);
        }

        private void AltoMovement()
        {
            rigidBody.AddForce(altoDirection * altoMagnitude * (AltoSpeed * userSpeed * Time.deltaTime));

            userVelocity = Mathf.RoundToInt(Mathf.Lerp(0, 9, rigidBody.velocity.magnitude / 6f));
        }

        private void UserSpeed()
        {
            if (jetPackControl.padPressed)
            {
                Vector2 padPosition = jetPackControl.padPosition;

                if(padPosition.x >= -0.8f && padPosition.x <= 0.8f)
                {
                    if(padPosition.y > 0.5f)
                    {
                        if(userSpeed < 5.0f) { userSpeed += UserSpeedIncremenet; }  
                    }
                    else if (padPosition.y < -0.5f)
                    {
                        if (userSpeed > 0.25f) { userSpeed -= UserSpeedIncremenet; }
                    }
                }
            }
        }

        private void StopAlto()
        {
            RunHapticFeedback("StopMotor1");
            RunHapticFeedback("StopMotor2");
            userVelocity = 0;
            rigidBody.velocity = Vector3.zero;
            rigidBody.angularVelocity = Vector3.zero;
        }

        private void ProcessAltoState()
        {
            //use state to control what movement can occur
            if (altoState != AltoBoardState.Static)
            {
                AltoMovement();
                UserSpeed();
                //checks which surface Alto is on
                CheckSurface();

                if (altoState == AltoBoardState.DynamicJetPack)
                {
                    JetPackMovement(jetPackControl.triggerPressed);
                    CheckFalling();
                }
            }
            else
            {
                StopAlto();
                if (jetPackControl.padPressed)
                {
                    altoState = AltoBoardState.DynamicJetPack;
                    RunHapticFeedback("Impact");
                    StartCoroutine(StopHapticTimer(1.0f, "StopMotor2"));
                }
            }
        }

        private void JetPackMovement(bool controllerEvent)
        {
            /*if (controllerEvent)
            {
                rigidBody.AddForce(new Vector3(0, (AltoHoverForce * userSpeed), 0));
                //controller haptics
                jetPackControl.TriggerHapticPulse(1000);
            }*/
        }

        private void CheckFalling()
        {
            float difference = gameObject.transform.position.y - previousYPos;
            if (difference < 0) { rigidBody.AddForce(new Vector3(0, -20.0f, 0)); }
            previousYPos = gameObject.transform.position.y;
        }

        private void CheckSurface()
        {
            RaycastHit raycast;
            if (Physics.Raycast(AltoPuck.transform.position, Vector3.down, out raycast, 1.25f))
            {
                if (hovering)
                {
                    RunHapticFeedback("Impact");
                    hovering = false;
                    if (!impacting) { impacting = true; StartCoroutine(ImpactCounter()); }
                }
                if (!impacting)
                {
                    if (raycast.transform.tag == "Water") { RunHapticFeedback("Water"); }
                    if (raycast.transform.tag == "Terrain") { RunHapticFeedback("Ground"); }
                }
            }
            else
            {
                hovering = true;
                RunHapticFeedback("Hover");
            }
        }

        //checks if Alto has collided with an object using a trigger collider on the arrow gameobject
        public void CheckCollision(string message)
        {
            //for trees, rocks, etc
            if(message == "Impact" && !impacting)
            { 
                RunHapticFeedback("Impact");
                impacting = true;
                StartCoroutine(ImpactCounter());
            }
            if(message == "Ammo" && !armed)
            {
                //can shoot now, game starts
                rightController.SetActive(false);
                gunObject.SetActive(true);
                guiArmObject.GetComponent<Canvas>().enabled = true;
                armed = true;
            }
        }

        private void CheckPlayerPresent()
        {
            if (altoMagnitude >= 0.75f)
            {
                if (checkingInactivity) { StopCoroutine(InactivePlayerCheck()); checkingInactivity = false; }
                altoInactive = false;
            }
            else { if (!checkingInactivity && !altoInactive) { checkingInactivity = true; StartCoroutine(InactivePlayerCheck()); } }
        }

        /// Timer coroutines ///
        
        IEnumerator InactivePlayerCheck()
        {
            yield return new WaitForSeconds(3.0f);
            checkingInactivity = false;
            StopAlto();
            altoInactive = true;
        }

        IEnumerator ImpactCounter()
        {
            yield return new WaitForSeconds(0.75f);
            impacting = false;
        }

        IEnumerator StopHapticTimer(float time, string motor)
        {
            yield return new WaitForSeconds(time);
            RunHapticFeedback(motor);
        }

        ///Alto Haptic Control Functions///

        //can call this function externally as well
        public void RunHapticFeedback(string stateName, bool power=false)
        {
            //checks for standard haptic states, can put in custom haptic states here
            switch (stateName)
            {
                case "Ground":
                    HapticFeedback("StopMotor2", 0);
                    HapticFeedback(stateName, userVelocity);
                    break;
                case "Water":
                    //HapticFeedback("StopMotor2", 0); 
                    //regular pulses with erratic pules on one motor and erratic pulses on the other motor
                    waterCounter += Time.deltaTime;
                    if (waterCounter > 2.0f) { waterCounter = 0.0f; }
                    int wrand = (int)(Random.Range(0, 6));
                    HapticFeedback("Hover", userVelocity * wrand);
                    HapticFeedback(stateName, userVelocity * (int)waterCounter * wrand);
                    break;
                case "Hover":
                    HapticFeedback("StopMotor1", 0);
                    //erratic pulses, but within a weak range
                    int hrand = (int)(Random.Range(0, 5));
                    if (userVelocity <= 1) { hrand = 0; }
                    HapticFeedback(stateName, userVelocity * hrand);
                    break;
                case "Impact":
                    HapticFeedback("StopMotor1", 0);
                    if(power) { HapticFeedback(stateName, 9); }
                    //TODO: rebounding pulse effect
                    HapticFeedback(stateName, userVelocity * 2);
                    break;
                case "Vehicle":
                    HapticFeedback("StopMotor2", 0);
                    //regular pulses with erratic pulses on both motors - combine to make a steam engine pulse effect
                    waterCounter += Time.deltaTime;
                    if (waterCounter > 2.0f) { waterCounter = 0.0f; }
                    int vrand = (int)(Random.Range(0, 4));
                    HapticFeedback(stateName, userVelocity * vrand);
                    HapticFeedback(stateName, userVelocity * (int)waterCounter * 10);
                    break;
                case "StopMotor1":
                    HapticFeedback(stateName, 0);
                    break;
                case "StopMotor2":
                    HapticFeedback(stateName, 0);
                    break;
                default:
                    //for generic custom haptic states
                    HapticFeedback(stateName, userVelocity);
                    break;
            }
        }

        //this function called internally
        private void HapticFeedback(string name, int intensity)
        {
            AltoInput.SendHapticCommand(name, intensity);
        }
        ///End Haptic Controls///

        void Update()
        {
            if (AltoInput.IsPresent())
            {
                ProcessAltoInput();
                if (!altoInactive)
                {
                    ProcessAltoState();
                }
            }
            else
            {
                Debug.Log("Cannot detect Alto, check port and Alto.cfg");
            }
        }
    }
}

